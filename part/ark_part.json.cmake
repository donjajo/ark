{
    "KPlugin": {
        "Description": "Archive Handling Tool",
        "Description[ca@valencia]": "Eina de gestió d'arxius",
        "Description[ca]": "Eina de gestió d'arxius",
        "Description[eo]": "Arkivpritrakta Ilo",
        "Description[es]": "Herramienta de gestión de archivos comprimidos",
        "Description[eu]": "Artxiboak kudeatzeko tresna",
        "Description[ia]": "Instrumento de manear archivo",
        "Description[ka]": "არქივებთან სამუშაო ხელსაწყო",
        "Description[nl]": "Hulpprogramma voor het beheren van archieven",
        "Description[nn]": "Verktøy for arkivhandsaming",
        "Description[pt]": "Ferramenta de Tratamento de Pacotes",
        "Description[sl]": "Orodje za upravljanje arhivov",
        "Description[tr]": "Arşiv İşleme Aracı",
        "Description[uk]": "Інструмент для роботи з архівами",
        "Description[x-test]": "xxArchive Handling Toolxx",
        "Icon": "ark",
        "License": "GPL v2+",
        "MimeTypes": [
            "@SUPPORTED_ARK_MIMETYPES_JSON@"
        ],
        "Name": "Archiver",
        "Name[ca@valencia]": "Arxivador",
        "Name[ca]": "Arxivador",
        "Name[eo]": "Arkivilo",
        "Name[es]": "Archivador",
        "Name[eu]": "Artxibatzailea",
        "Name[ia]": "Archivaor",
        "Name[ka]": "არქივატორი",
        "Name[nl]": "Archiefgereedschap",
        "Name[nn]": "Arkivprogram",
        "Name[pt]": "Ark",
        "Name[ru]": "Архиватор",
        "Name[sl]": "Arhivar",
        "Name[ta]": "காப்பக உருவாக்கி",
        "Name[tr]": "Arşivleyici",
        "Name[uk]": "Архіватор",
        "Name[x-test]": "xxArchiverxx",
        "ServiceTypes": [
            "KParts/ReadOnlyPart",
            "Browser/View"
        ],
        "Version": "@ARK_VERSION@",
        "Website": "https://utils.kde.org/projects/ark"
    }
}
