{
    "KPlugin": {
        "MimeTypes": [
            "@SUPPORTED_ARK_MIMETYPES_JSON@"
        ],
        "Name": "Ark Extract Here",
        "Name[ca@valencia]": "Extracció d'Ark ací",
        "Name[ca]": "Extracció de l'Ark aquí",
        "Name[eo]": "Ark-Extrakti ĉi tie",
        "Name[es]": "Ark - Extraer aquí",
        "Name[eu]": "Ark Erauzi hemen",
        "Name[ia]": "Ark Extrahe Hic",
        "Name[ka]": "Ark აქ გაშლა",
        "Name[nl]": "Ark: hier uitpakken",
        "Name[pt]": "Ark - Extrair Aqui",
        "Name[ru]": "Распаковать в эту папку",
        "Name[sl]": "Arkov ekstrakt tukaj",
        "Name[tr]": "Ark Buraya Çıkar",
        "Name[uk]": "Розпакувати сюди за допомогою Ark",
        "Name[x-test]": "xxArk Extract Herexx"
    }
}
